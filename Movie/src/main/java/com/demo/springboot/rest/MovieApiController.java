package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.demo.springboot.service.MovieService;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    MovieService service;

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies(@RequestParam(defaultValue = "") String search) {

        LOGGER.info("--- get movies");

        String csvFile = "src/main/resources/movies.csv";

        return service.getMovies(csvFile, search);
    }
}
