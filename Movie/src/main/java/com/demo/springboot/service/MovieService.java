package com.demo.springboot.service;

import com.demo.springboot.domain.dto.MovieListDto;

public interface MovieService {
    MovieListDto getMovies(String path, String search);
}
