package com.demo.springboot.service.impl;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.springframework.stereotype.Service;
import com.demo.springboot.service.MovieService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    @Override
    public MovieListDto getMovies(String path, String search) {
        List<MovieDto> movies = new ArrayList<>();
        List<String> list = new ArrayList<>();
        List<String> words;
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                list.add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (String line : list) {
            words = Arrays.asList(line.split(";"));
            if (!words.get(4).trim().equals("true") && (words.get(1).toLowerCase().trim().contains(search.toLowerCase()))) {
                movies.add(new MovieDto(Integer.parseInt(words.get(0).trim()), words.get(1).trim(), Integer.parseInt(words.get(2).trim()), words.get(3).trim()));
            }
        }
        return new MovieListDto(movies);
    }
}
